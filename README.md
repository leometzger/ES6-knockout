# ES6 Knockout

Esse código é o resultado do post no medium sobre a utilização do **KnockoutJS** em conjunto do **Ecmascript2015**.

### Run

Para rodar este código precisa clonar o repositório e rodar os seguintes comandos:

    npm install
    npm run dev

E já pode ver o exemplo rodando no endereço http://localhost:3000
    