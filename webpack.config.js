const webpack = require('webpack');

module.exports = {
    entry: ['./src/index.js'],
    output: {
      filename: './src/bundle.js'
    },
    devServer: {
        inline: true,
        contentBase: './src',
        port: 3000
    },
    module:{
        loaders:[{
            test: /\.js$/,
            exclude: /(node_modules)/,
            loader: 'babel-loader',
            query:{
                presets: ['es2015']
            }
        }]
    }
};
