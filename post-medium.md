# Utilizando KnockoutJS com ES6+

Recentemente me surgiu a necessidade de criar um POC (*proof of concept*) de uma aplicação utilizando knockoutjs. Na ocasião, pensei porque não utilizá-lo com Ecmascript2015(ES6)?

A utilização dele com o ES6 não é muito comum. Por ser uma biblioteca mais antiga e, ser usado em menos projetos, normalmente é utilizado o javascript vanilla. Inclusive a documentação do knockoutjs só tem exemplos usando o vanilla.

Percebi que não tinham muitos fontes sobre o uso do knockout com o ES6. Então decidi escrever esse post para caso alguém ainda utilize o knockout em seus projetos e queira utilizar em conjunto com o javascript mais novo.

Para isso, vamos utilizar o webpack, npm e o babel para configurar nosso projeto.

(OBS: Esse post não tem como objetivo se aprofundar muito sobre webpack, apenas a configuração básica para usar o knockout. Inclusive foi deixado alguns links para quem quiser aprender mais sobre o assunto.)

Entre no diretório do projeto e digite os seguintes comandos:

```bash
npm init -y
npm install -g webpack
npm install --save-dev webpack
npm install --save-dev babel-core babel-loader babel-preset-es2015
npm install
```

Em seguida podemos criar nosso arquivo de webpack. Crie o arquivo `webpack.config.js`.

(Caso queira saber mais sobre [webpack](https://webpack.js.org/guides/getting-started/).)

```javascript
const webpack = require('webpack');

module.exports = {
    entry: ['./src/index.js'],
    output: {
      // local que queira que seja gerado o bundle
      filename: './src/bundle.js' 
    },
    devServer: {
        inline: true,
        contentBase: './src',
        port: 3000
    },
    module:{
        loaders:[{
            test: /\.js$/,
            exclude: /(node_modules)/,
            loader: 'babel-loader',
            query:{
                presets: ['es2015']
            }
        }]
    }
};
```

(Sugiro que procure um pouco sobre o webpack-dev-server também, pois com ele é possível fazer hot-reload e deixar seu trabalho muito mais produtivo).

Com esta configuração, os arquivos do projeto, html e javascript, foram criados dentro da pasta `src`, caso queira que o webpack gere o bundle a partir de outro diretório, ou para outro, lembre de trocar o nome do diretório no arquivo de configuração.

Para indicar qual a versão de javascript estamos usando, crie também o arquivo `.babelrc`, com o seguinte conteúdo:

```javascript
{
    "presets": ["es2015"]
}
```

A partir daqui, os arquivos descritos a seguir serão criados na pasta `src`.

Podemos fazer a instalação do knockoutjs de diferentes formas. Para simplificar, preferi fazer o download do arquivo. [Instalação](http://knockoutjs.com/downloads/index.html).

Após essa configuração inicial, podemos criar nosso arquivo `index.html`.

```html
<html>

    <head>
        <title>Knockout com ES6</title>
    </head>

    <body>
                

        <script type='text/javascript' src='knockout.js'></script>
        <script src="bundle.js"></script>
    </body>

</html>
```

Perceba que estamos referenciando o `bundle.js`, que é o arquivo criado pelo webpack. Este arquivo "compilado" está utilizando o javascript vanilla, ou seja, é possível rodá-lo em qualquer browser.

Também estamos referenciando o arquivo do **knockout.js**, que baixamos anteriormente.

Com isso feito, já podemos criar nossa primeira VM, utilizando o ES6. Basta criar o arquivo `index.js`.

```javascript
class IndexVM {

    constructor(){

    }
}

ko.applyBindings(new IndexVM());
```

Veja, ja estamos usando features do ES6. Porém antes de ver o arquivo index.html funcionando, precisamos dar o comando `webpack` no terminal, dentro da pasta raiz, para gerar nosso arquivo "compilado". 

A partir daí, nosso arquivo `index.html` já pode ser aberto. Ele ja está utilizando o javascript gerado com webpack do ES6.

Como as aplicações com **knockout** normalmente são MPAs (Multiple Page Applications), sugiro que visite este [link](https://webpack.github.io/docs/multiple-entry-points.html) onde explica a configuração do webpack com multiplos *entrypoints*. Como o post não é sobre webpack, não vou me aprofundar muito.

Com isso temos um javascript atual, com importação de modulos, classes, e todas as outras vantagens que o Ecmascript2015 (ES6) tem para nos oferecer. Deixando o código mais limpo e reutilizável.

Espero que isso ajude alguém que ainda venha a utilizar o knockoutjs (apesar de ser raro hoje em dia). 

É possível ver o código fonte do post no [Gitlab](https://gitlab.com/leometzger/ES6-knockout).