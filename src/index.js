class IndexVM {
    
    constructor(){
        this.count = ko.observable(0);
    }

    increment() {
        console.log("increment");        
        this.count(this.count() + 1);
    }

    decrement() {
        console.log("decrement");
        this.count(this.count() - 1);
    }
}

ko.applyBindings(new IndexVM());